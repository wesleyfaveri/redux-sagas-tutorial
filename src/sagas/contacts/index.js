import { takeEvery, put, call } from 'redux-saga/effects';
import types from '../../reducers/contacts/types';
import typesTags from '../../reducers/tags/types';
import { getContacts } from '../../sdk/contacts';

function* fetchGetContacts(action) {
  try {
    const { contacts } = yield call(getContacts);

    //yield put({ type: typesTags.GET });

    yield put({ type: types.GET_SUCCESS, contacts });
  } catch (err) {
    yield put({ type: types.GET_ERROR, error: err.message });
  }
}

export default [
  takeEvery(types.GET, fetchGetContacts),
];
