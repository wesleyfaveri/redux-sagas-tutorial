import { all } from 'redux-saga/effects';
import contacts from './contacts';

function* rootSaga() {
  yield all([
    ...contacts,
  ]);
}

export default rootSaga;
