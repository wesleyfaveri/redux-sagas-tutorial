import React, { useEffect } from 'react';
import './App.css';
import { connect } from 'react-redux';

import { getContacts } from './reducers/contacts';

const App = ({ contacts, onGetContacts }) => {
  useEffect(() => {
    onGetContacts();

    return () => null;
  }, []);

  return (
    <div className="App">
      {contacts.map((item, index) => (
        <div key={index}>
          <p>{item.name}</p>
        </div>
      ))}
    </div>
  );
}

const mapStateToProps = ({ contacts }) => ({
  contacts: contacts.get('contacts'),
});

const mapDispatchToProps = (dispatch) => ({
  onGetContacts: () => dispatch(getContacts()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
