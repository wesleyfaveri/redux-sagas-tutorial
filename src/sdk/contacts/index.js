import { get } from '../api';

export const getContacts = () => {
  return get({
    path: 'contacts?offset=0&page_size=100&types=all',
  });
};
