import { HTTP_GET, HTTP_PUT, HTTP_POST, HTTP_DELETE } from './types';
import HEADER_KEYS from './headerDefault';

export const DEFAULT_ERROR_MESSAGE = 'Não foi possível processar a requisição';

const getHeaders = (header = {}, authToken = {}) => ({
  'Content-Type': HEADER_KEYS.DEFAULT_CONTENT_TYPE,
  'Accept': HEADER_KEYS.DEFAULT_ACCEPT,
  ...header,
  ...authToken,
});

const handleResponse = async (response, request) => {
  let object = await response.text();

  object = JSON.parse(object);

  if (response.ok) {
    return object;
  }

  throw object;
};

const request = ({ path = '', method, body }) => {
  let url = `${path}`;

  const headers = getHeaders();

  return fetch(url, { method, headers, body: JSON.stringify(body) }).then((response) => handleResponse(response, { url, method, headers, body }));
};

export const get = (params) => request({
  ...params,
  method: HTTP_GET,
});

export const post = (params) => request({
  ...params,
  method: HTTP_POST,
});

export const put = (params) => request({
  ...params,
  method: HTTP_PUT,
});

export const del = (params) => request({
  ...params,
  method: HTTP_DELETE,
});
