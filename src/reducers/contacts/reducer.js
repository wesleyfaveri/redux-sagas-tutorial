import { Map } from 'immutable';
import types from './types';

const initialState = Map({
  loadingGet: false,
  successGet: false,
  errorGet: false,

  contacts: [],
});

const handleGetContacts = (state, action) => {
  return state
    .set('loadingGet', true)
    .set('successGet', false)
    .set('errorGet', false);
};

const handleGetContactsSuccess = (state, action) => {
  return state
    .set('contacts', action.contacts)
    .set('loadingGet', false)
    .set('successGet', true)
    .set('errorGet', false);
};

const handleGetContactsError = (state, action) => {
  return state
    .set('loadingGet', false)
    .set('successGet', false)
    .set('errorGet', action.error);
};

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case types.GET:
      return handleGetContacts(state, action);
    case types.GET_SUCCESS:
      return handleGetContactsSuccess(state, action);
    case types.GET_ERROR:
      return handleGetContactsError(state, action);

    default:
      return state;
  }
};

export default reducer;
