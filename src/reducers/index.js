import { combineReducers } from 'redux';
import contactsReducer from './contacts';
import tagsReducer from './tags';

const reducers = combineReducers({
  contacts: contactsReducer,
  tags: tagsReducer,
});

export default reducers;
