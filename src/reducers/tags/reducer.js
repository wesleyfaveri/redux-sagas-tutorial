import { Map } from 'immutable';
import types from './types';

const initialState = Map({
  loadingGet: false,
  successGet: false,
  errorGet: false,

  tags: [],
});

const handleGetTags = (state, action) => {
  return state
    .set('loadingGet', true)
    .set('successGet', false)
    .set('errorGet', false);
};

const handleGetTagsSuccess = (state, action) => {
  return state
    .set('tags', action.tags)
    .set('loadingGet', false)
    .set('successGet', true)
    .set('errorGet', false);
};

const handleGetTagsError = (state, action) => {
  return state
    .set('loadingGet', false)
    .set('successGet', false)
    .set('errorGet', action.error);
};

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case types.GET:
      return handleGetTags(state, action);
    case types.GET_SUCCESS:
      return handleGetTagsSuccess(state, action);
    case types.GET_ERROR:
      return handleGetTagsError(state, action);

    default:
      return state;
  }
};

export default reducer;
